#include <iostream>

using namespace std;

void Add(char a2[], char b2[], char res[], long long int a_size, long long int b_size);
void Sub(char a2[], char b2[], char res[], long long int a_size, long long int b_size);
void Mul(char a2[], char b2[], char res[], long long int a_size, long long int b_size);
void Div(char a2[], char b2[], char q[], char r[], long long int a_size, long long int b_size);

int main(int argc, char* argv[])
{
	FILE *f = fopen(argv[1], "r");
	fseek(f, 0, SEEK_END);
	long long int a_size = ftell(f);
	char * a;
	a = (char*) malloc (a_size + 1);
	fseek(f, 0, SEEK_SET);
	fscanf(f, "%s", a);
	fclose(f);
	f = fopen(argv[3], "r");
	fseek(f, 0, SEEK_END);
	long long int b_size = ftell(f);
	char * b;
	b = (char*) malloc (b_size + 1);
	fseek(f, 0, SEEK_SET);
	fscanf(f, "%s", b);
	fclose(f);
	char * res;
	res = (char*) malloc (a_size + b_size + 10);
	char * temp;
	int division = 0;
	switch (*argv[2])
	{
	case '+':
		{
			Add(a, b, res, a_size, b_size);
		}
		break;
	case '-':
		{
			Sub(a, b, res, a_size, b_size);
		}
		break;
	case '*':
		{
			Mul(a, b, res, a_size, b_size);
		}
		break;
	case '/':
		{
			temp = (char*) malloc (a_size + b_size + 10);
			Div(a, b, res, temp, a_size, b_size);
			division = 1;
		}
		break;
	case '%':
		{
			temp = (char*) malloc (a_size + b_size + 10);
			Div(a, b, res, temp, a_size, b_size);
			division = 2;
		}
		break;
	}

	f = fopen(argv[4], "w");
	if (division == 2)
		fprintf(f, "%s", temp);
	else
		fprintf(f, "%s", res);
	fclose(f);
	free(a);
	a = NULL;
	free(b);
	b = NULL;
	free(res);
	res = NULL;
	if (division == 1 || division == 2)
	{
		free(temp);
		temp = NULL;
	}
    return 0;
}



void Add(char a2[], char b2[], char res[], long long int a_size, long long int b_size)
{
	int i, j, qq, k, s, w;
	char * a;
	a = (char*) malloc (a_size + b_size + 5);
	char * b;
	b = (char*) malloc (a_size + b_size + 5);
	char * temp;
	strcpy(a, a2);
	strcpy(b, b2);
	bool Aminus = false;
	bool Bminus = false;
	if (a[0] == '-')
	{
		Aminus = true;
		for (i = 0; i < strlen(a); i++)
		{
			a[i] = a[i+1];
		}
	}
	if (b[0] == '-')
	{
		Bminus = true;
		for (i = 0; i < strlen(b); i++)
		{
			b[i] = b[i+1];
		}
	}
	if(Aminus && !Bminus)
	{
		Sub(b, a, res, a_size, b_size);
	}
	if(!Aminus && Bminus)
	{
		Sub(a, b, res, a_size, b_size);
	}
	if( (Bminus && Aminus) || (!Bminus && !Aminus))
	{
		long long int na = a_size, nb = b_size;
		if (nb > na)
		{
			temp = (char*) malloc (a_size + b_size + 10);
			strcpy(temp, a);
			strcpy(a, b);
			strcpy(b, temp);
			free(temp);
			long long int temp2 = na;
			na = nb;
			nb = temp2;
		}
		res[na] = 0;
		int carry = 0;
		for (i = na - 1; i >= 0; i--)
		{
			if (i >= na - nb)
			{
				if (((int) a[i] - 48) + ((int) b[i - (na - nb)] - 48) + carry < 10)
				{
					res[i] = ((int) a[i] - 48) + ((int) b[i - (na - nb)] - 48) + carry + 48;
					carry = 0;
				}
				else
				{
					res[i] = (((int) a[i] - 48) + ((int) b[i - (na - nb)] - 48) + carry) % 10 + 48;
					carry = 1;
				}
			}
			else
			{
				if (((int) a[i] - 48) + carry < 10)
				{
					res[i] = (int) a[i] + carry;
					carry = 0;
					for (j = i - 1; j >= 0; j--)
					{
						res[j] = a[j];
					}
					break;
				}
				else
				{
					res[i] = (((int) a[i] - 48) + carry) % 10 + 48;
					carry = 1;
				}
			}
		}
		if (carry == 1)
		{
			temp = (char*) malloc (a_size + b_size + 10);
			temp[0] = '1';
			for (qq = 0; qq <= strlen(res); qq++)
			{
				temp[qq + 1] = res[qq];
			}
			strcpy(res, temp);
			free(temp);
		}
	}
	if(Aminus && Bminus)
	{
		temp = (char*) malloc (a_size + b_size + 10);
		temp[0] = '-';
		for (i = 0; i <= strlen(res); i++)
		{
			temp[i+1] = res[i];
		}
		strcpy(res, temp);
		free(temp);
	}
	free(a);
	free(b);
}

void Sub(char a2[], char b2[], char res[], long long int a_size, long long int b_size)
{
	int i, j, qq, k, s, w;
	char * a;
	a = (char*) malloc (a_size + b_size + 5);
	strcpy(a, a2);
	char * b;
	b = (char*) malloc (a_size + b_size + 5);
	strcpy(b, b2);
	char * temp;
	bool Aminus = false;
	bool Bminus = false;
	if (a[0] == '-')
	{
		Aminus = true;
		for (i = 0; i < strlen(a); i++)
		{
			a[i] = a[i+1];
		}
	}
	if (b[0] == '-')
	{
		Bminus = true;
		for (i = 0; i < strlen(b); i++)
		{
			b[i] = b[i+1];
		}
	}
	if (!Aminus && Bminus)
	{
		Add(a, b, res, (long long int) strlen(a), (long long int) strlen(b));
	}
	if (Aminus && !Bminus)
	{
		Add(a, b, res, (long long int) strlen(a), (long long int) strlen(b));
		temp = (char*) malloc (a_size + b_size + 10);
		temp[0] = '-';
		for (i = 0; i <= strlen(res); i++)
		{
			temp[i + 1] = res[i];
		}
		strcpy(res, temp);
		free(temp);
	}
	if (Aminus && Bminus)
	{
		Sub(b, a, res, (long long int) strlen(b), (long long int) strlen(a));
	}
	if (!Aminus && !Bminus)
	{
		int na = strlen(a), nb = strlen(b);
		bool isBigger = false;
		if (nb > na)
		{
			temp = (char*) malloc (a_size + b_size + 10);
			strcpy(temp, a);
			strcpy(a, b);
			strcpy(b, temp);
			free(temp);
			int temp2 = na;
			na = nb;
			nb = temp2;
			isBigger = true;
		}
		else
			if (nb == na)
			{
				for (i = 0; i < na; i++)
				{
					if ((int) b[i] > (int) a[i])
					{
						isBigger = true;
						break;
					}
					else
						if ((int) b[i] < (int) a[i])
						{
							isBigger = false;
							break;
						}
				}
				if (isBigger)
				{
					temp = (char*) malloc (a_size + b_size + 10);
					strcpy(temp, a);
					strcpy(a, b);
					strcpy(b, temp);
					free(temp);
					int temp2 = na;
					na = nb;
					nb = temp2;
				}
			}
		for (i = 0; i < na / 2; i++)
		{
			char tempc = a[i];
			a[i] = a[na - 1 - i];
			a[na - 1 - i] = tempc;
		}
		for (i = 0; i < nb / 2; i++)
		{
			char tempc = b[i];
			b[i] = b[nb - 1 - i];
			b[nb - 1 - i] = tempc;
		}
		for (i = 0; i < nb; i++)
		{
			if ((int) a[i] >= (int) b[i])
			{
				res[i] = (int) a[i] - (int) b[i] + 48;
			}
			else
			{
				int j = i + 1;
				while (a[j] == '0')
				{
					j++;
				}
				a[j]--;
				for (k = j - 1; k > i; k--)
				{
					a[k] = '9';
				}
				res[i] = (int) a[i] + 10 - (int) b[i] + 48;
			}
		}
		for (i = nb; i < na; i++)
		{
			res[i] = a[i];
		}
		res[na] = 0;
		int k = na - 1;
		while (res[k] == '0' && k != 0)
		{
			res[k] = 0;
			k--;
		}
		if (isBigger)
		{
			res[k + 1] = '-';
			res[k + 2] = 0;
		}
		for (i = 0; i < strlen(res) / 2; i++)
		{
			char tempc = res[i];
			res[i] = res[strlen(res) - 1 - i];
			res[strlen(res) - 1 - i] = tempc;
		}
	}
	free(a);
	free(b);
}

void Mul(char a2[], char b2[], char res[], long long int a_size, long long int b_size)
{
	int i, j, qq, k, s, w;
	char * a;
	a = (char*) malloc (a_size + b_size + 5);
	strcpy(a, a2);
	char * b;
	b = (char*) malloc (a_size + b_size + 5);
	strcpy(b, b2);
	char * temp;
	bool Aminus = false;
	bool Bminus = false;
	if (a[0] == '-')
	{
		Aminus = true;
		for (i = 0; i < strlen(a); i++)
		{
			a[i] = a[i+1];
		}
	}
	if (b[0] == '-')
	{
		Bminus = true;
		for (i = 0; i < strlen(b); i++)
		{
			b[i] = b[i+1];
		}
	}
	int na = strlen(a), nb = strlen(b);
	bool isBigger = false;
	if (nb > na)
	{
		temp = (char*) malloc (a_size + b_size + 10);
		strcpy(temp, a);
		strcpy(a, b);
		strcpy(b, temp);
		free(temp);
		int temp2 = na;
		na = nb;
		nb = temp2;
		isBigger = true;
	}
	else
		if (nb == na)
		{
			for (i = 0; i <= na; i++)
			{
				if ((int) b[i] > (int) a[i])
				{
					isBigger = true;
					break;
				}
			}
			if (isBigger)
			{
				temp = (char*) malloc (a_size + b_size + 10);
				strcpy(temp, a);
				strcpy(a, b);
				strcpy(b, temp);
				free(temp);
				int temp2 = na;
				na = nb;
				nb = temp2;
			}
		}
	for (i = 0; i < na / 2; i++)
	{
		char tempc = a[i];
		a[i] = a[na - 1 - i];
		a[na - 1 - i] = tempc;
	}
	for (i = 0; i < nb / 2; i++)
	{
		char tempc = b[i];
		b[i] = b[nb - 1 - i];
		b[nb - 1 - i] = tempc;
	}
	for (i = 0; i < na + nb; i++)
	{
		res[i] = '0';
	}
	for (j = 0; j < nb; j++)
	{
		if (b[j] != '0')
		{
			int k = 0;
			for (i = 0; i < na; i++)
			{
				int t = ((int) a[i] - 48) * ((int) b[j] - 48) + ((int)res[i + j] - 48) + k;
				k = t / 10;
				res[i + j] = t % 10 + 48;
			}
			res[j + na] = k + 48;
		}
	}
	res[na + nb] = 0;
	while((res[strlen(res) - 1] == '0') && (strlen(res) != 1))
	{
		res[strlen(res) - 1] = 0;
	}
	for (i = 0; i < strlen(res) / 2; i++)
	{
		char temp = res[i];
		res[i] = res[strlen(res) - 1 - i];
		res[strlen(res) - 1 - i] = temp;
	}
	if( (Aminus && !Bminus) || (!Aminus && Bminus))
	{
		if (res[0] != '0')
		{
			temp = (char*) malloc (a_size + b_size + 10);
			temp[0] = '-';
			for (i = 0; i <= strlen(res); i++)
			{
				temp[i + 1] = res[i];
			}
			strcpy(res, temp);
			free(temp);
		}
	}
	free(a);
	free(b);
}

void Div(char a2[], char b2[], char q[], char r[], long long int a_size, long long int b_size)
{
	int i, j, qq, k, s, w;
    int d;
	char d_char[3];
	char * res;
	char * a;
	a = (char*) malloc (a_size + b_size + 5);
	strcpy(a, a2);
	char * b;
	b = (char*) malloc (a_size + b_size + 5);
	strcpy(b, b2);
	char * temp;
	bool Aminus = false;
	bool Bminus = false;
	if (a[0] == '-')
	{
		Aminus = true;
		for (i = 0; i < strlen(a); i++)
		{
			a[i] = a[i+1];
		}
	}
	if (b[0] == '-')
	{
		Bminus = true;
		for (i = 0; i < strlen(b); i++)
		{
			b[i] = b[i+1];
		}
	}
	int na = strlen(a), nb = strlen(b);
	if (nb == 1)
	{
		d = b[0] - 48;
		char * del;
		del = (char*) malloc (a_size + b_size + 5);
		strcpy(del, a);
		for (s = 0; s < strlen(del); s++)
		{
			if (s == 0)
			{
				r[s] = ((int) del[0] - 48) / d + 48;
				del[s] = ((int) del[0] - 48) - ((((int) del[0] - 48) / d) * d) + 48;
			}
			else
			{
				int pp = (((int) del[s - 1] - 48) * 10 + ((int) del[s] - 48)) / d;
				r[s] = pp + 48;
				pp = (((int) del[s - 1] - 48) * 10 + ((int) del[s] - 48)) - pp * d;
				if (pp / 10 == 0)
				{
					del[s - 1] = '0';
					del[s] = pp + 48;
				}
				else
				{
					del[s - 1] = pp / 10 + 48;
					del[s] = pp % 10 + 48;
				}
			}
		}
		r[strlen(del)] = 0;
		bool tralivali = false;
		while(!tralivali)
		{
			if (r[0] == '0' && strlen(r) > 1)
			{
				temp = (char*) malloc (a_size + b_size + 5);
				strncpy(temp, &r[1], strlen(r));
				strcpy(r, temp);
				free(temp);
			}
			else
				tralivali = true;
		}
		strcpy(q, r);
		r[0] = del[strlen(del) - 1];
		r[1] = 0;
		free(del);
	}
	else
	{
		if (nb > na)
		{
			q[0] = '0';
			q[1] = 0;
			strcpy(r, a);
		}
		else
		{
			temp = (char*) malloc (a_size + b_size + 5);
			Sub(a, b, temp, (long long int) strlen(a), (long long int) strlen(b));
			char TEMPESTO = temp[0];
			free(temp);
			if (TEMPESTO == '-')
			{
				q[0] = '0';
				q[1] = 0;
				strcpy(r, a);
			}
			else
			{
				d = 10 / ((int) b[0] - 48 + 1);
				if (d != 1)
				{
					if (d == 10)
					{
						d_char[0] = '1';
						d_char[1] = '0';
						d_char[2] = 0;
					}
					else
					{
						d_char[0] = d + 48;
						d_char[1] = 0;
					}
					res = (char*) malloc (a_size + b_size + 5);
					Mul(a, d_char, res, (long long int) strlen(a), (long long int) strlen(d_char));
					strcpy(a, res);
					free(res);
					if (na == strlen(a))
					{
						temp = (char*) malloc (a_size + b_size + 5);
						temp[0] = '0';
						for (i = 0; i <= strlen(a); i++)
						{
							temp[i + 1] = a[i];
						}
						strcpy(a, temp);
						free(temp);
					}
					res = (char*) malloc (a_size + b_size + 5);
					Mul(b, d_char, res, (long long int) strlen(b), (long long int) strlen(d_char));
					strcpy(b, res);
					free(res);
				}
				if (d == 1)
				{
					temp = (char*) malloc (a_size + b_size + 5);
					temp[0] = '0';
					for (i = 0; i <= strlen(a); i++)
					{
						temp[i + 1] = a[i];
					}
					strcpy(a, temp);
					free(temp);
				}

				for (j = na - nb; j >= 0; j--)
				{
					int qq3 = (((int) a[na - nb - j] - 48) * 10 + ((int) a[na - nb - j + 1] - 48)) / ((int) b[0] - 48);
					int rr = (((int) a[na - nb - j] - 48) * 10 + ((int) a[na - nb - j + 1] - 48)) % ((int) b[0] - 48);
					if (qq3 == 10 || qq3 * ((int) b[1] - 48) > (10 * rr + ((int) a[na - nb - j + 2] - 48)))
					{
						qq3--;
						rr += (int) b[0] - 48;
						if (rr < 10)
						{
							if (qq3 == 10 || qq3 * ((int) b[1] - 48) > (10 * rr + ((int) a[na - nb - j + 2] - 48)))
							{
								qq3--;
								rr += (int) b[0] - 48;
							}
						}
					}
					char qq_char[3];
					if (qq3 / 10 != 0)
					{
						qq_char[0] = qq3 / 10 + 48;
						qq_char[1] = qq3 % 10 + 48;
						qq_char[2] = 0;
					}
					else
					{
						qq_char[0] = qq3 + 48;
						qq_char[1] = 0;

					}
					res = (char*) malloc (a_size + b_size + 5);
					Mul(b, qq_char, res, (long long int) strlen(b), (long long int) strlen(qq_char));
					char * b_qq;
					b_qq = (char*) malloc (a_size + b_size + 5);
					strcpy(b_qq, res);
					free(res);
					char * a_jn;
					a_jn = (char*) malloc (a_size + b_size + 5);
					strncpy(a_jn, &a[na - nb - j], nb + 1);
					a_jn[nb + 1] = 0;
					res = (char*) malloc (a_size + b_size + 5);
					Sub(a_jn, b_qq, res, (long long int) strlen(a_jn), (long long int) strlen(b_qq));
					char * sub_res;
					sub_res = (char*) malloc (a_size + b_size + 5);
					strcpy(sub_res, res);
					free(res);
					if (sub_res[0] == '-')
					{
						qq3--;
						if (qq3 / 10 != 0)
						{
							qq_char[0] = qq3 / 10 + 48;
							qq_char[1] = qq3 % 10 + 48;
							qq_char[2] = 0;
						}
						else
						{
							qq_char[0] = qq3 + 48;
							qq_char[1] = 0;
						}
						res = (char*) malloc (a_size + b_size + 5);
						Mul(b, qq_char, res, (long long int) strlen(b), (long long int) strlen(qq_char));
						strcpy(b_qq, res);
						free(res);
						strncpy(a_jn, &a[na - nb - j], nb + 1);
						a_jn[nb + 1] = 0;
						res = (char*) malloc (a_size + b_size + 5);
						Sub(a_jn, b_qq, res, (long long int) strlen(a_jn), (long long int) strlen(b_qq));
						strcpy(sub_res, res);
						free(res);
					}
					if (sub_res[0] != '-')
					{
						if (strlen(sub_res) != strlen(a_jn))
						{
							int num = strlen(a_jn) - strlen(sub_res);
							char * sub_res2;
							sub_res2 = (char*) malloc (a_size + b_size + 5);
							for (qq = 0; qq < num; qq++)
							{
								sub_res2[qq] = '0';
							}
							for (qq = num; qq <= num + strlen(sub_res); qq++)
							{
								sub_res2[qq] = sub_res[qq - num];
							}
							strcpy(sub_res, sub_res2);
							free(sub_res2);
						}
					}
					q[na - nb - j] = qq3 + 48;
					free(a_jn);
					free(b_qq);
					if (sub_res[0] == '-')
					{
						q[na - nb - j]--;
						char * add_res;
						add_res = (char*) malloc (a_size + b_size + 5);
						strncpy(add_res, &sub_res[1], strlen(sub_res));
						res = (char*) malloc (a_size + b_size + 5);
						Add(add_res, b, res, (long long int) strlen(add_res), (long long int) strlen(b));
						strcpy(add_res, res);
						free(res);
						int iii = -1;
						for (w = na - nb - j; w < na - nb - j + nb + 1; w++)
						{
							iii++;
							a[w] = sub_res[iii];
						}
						for (w = nb + 1 - j; w < nb + 1 - j + nb + 1; w++)
						{
							a[w] = add_res[w - (nb + 1 - j)];
						}
						free(add_res);
					}
					else
					{
						int iii = -1;
						for (w = na - nb - j; w < na - nb - j + nb + 1; w++)
						{
							iii++;
							a[w] = sub_res[iii];
						}
					}
				}
				char * del;
				del = (char*) malloc (a_size + b_size + 5);
				int i4 = -1;
				for (i = strlen(a) - nb; i <= strlen(a); i++)
				{
					i4++;
					del[i4] = a[i];
				}
				if (del[0] == '0')
				{
					temp = (char*) malloc (a_size + b_size + 5);
					strncpy(temp, &del[1], strlen(del));
					strcpy(del, temp);
					free(temp);
				}
				if (d == 10)
				{
					del[strlen(del) - 1] = 0;
				}
				else
				{
					for (s = 0; s < strlen(del); s++)
					{
						if (s == 0)
						{
							r[s] = ((int) del[0] - 48) / d + 48;
							del[s] = ((int) del[0] - 48) - ((((int) del[0] - 48) / d) * d) + 48;
						}
						else
						{
							int pp = (((int) del[s - 1] - 48) * 10 + ((int) del[s] - 48)) / d;
							r[s] = pp + 48;
							pp = (((int) del[s - 1] - 48) * 10 + ((int) del[s] - 48)) - pp * d;
							if (pp / 10 == 0)
							{
								del[s - 1] = '0';
								del[s] = pp + 48;
							}
							else
							{
								del[s - 1] = pp / 10 + 48;
								del[s] = pp % 10 + 48;
							}
						}
					}
				}
				r[strlen(del)] = 0;
				bool tralivali = false;
				while(!tralivali)
				{
					if (r[0] == '0' && strlen(r) > 1)
					{
						temp = (char*) malloc (a_size + b_size + 5);
						strncpy(temp, &r[1], strlen(r));
						strcpy(r, temp);
						free(temp);
					}
					else
						tralivali = true;
				}
				q[na - nb + 1] = 0;
				if (strlen(q) > 1 && q[0] == '0')
				{
					temp = (char*) malloc (a_size + b_size + 5);
					strncpy(temp, &q[1], strlen(q));
					strcpy(q, temp);
					free(temp);
				}
				free(del);
			}
		}
	}
	if (Aminus && !Bminus)
	{
		if (q[0] != '0')
		{
			temp = (char*) malloc (a_size + b_size + 5);
			temp[0] = '-';
			for (i = 0; i <= strlen(q); i++)
			{
				temp[i + 1] = q[i];
			}
			strcpy(q, temp);
			free(temp);
		}
		if (r[0] != '0')
		{
			temp = (char*) malloc (a_size + b_size + 5);
			temp[0] = '-';
			for (i = 0; i <= strlen(r); i++)
			{
				temp[i + 1] = r[i];
			}
			strcpy(r, temp);
			free(temp);
		}
	}
	if (!Aminus && Bminus)
	{
		if (q[0] != '0')
		{
			temp = (char*) malloc (a_size + b_size + 5);
			temp[0] = '-';
			for (i = 0; i <= strlen(q); i++)
			{
				temp[i + 1] = q[i];
			}
			strcpy(q, temp);
			free(temp);
		}
	}
	if (Aminus && Bminus)
	{
		if (r[0] != '0')
		{
			temp = (char*) malloc (a_size + b_size + 5);
			temp[0] = '-';
			for (i = 0; i <= strlen(r); i++)
			{
				temp[i + 1] = r[i];
			}
			strcpy(r, temp);
			free(temp);
		}
	}
	free(a);
	free(b);
}